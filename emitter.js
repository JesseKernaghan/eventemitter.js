(function(w){
    "use strict";
    //attach to window
    w.EventEmitter = {
        //storage for all events
        events : {},

        parse : function( action, args ) {
            //store events array for iteration
            var events = args[0];

            //loop events and refire action with individual events
            for( var i = 0, l = events.length; i < l; i++ ){
                //reassign first argument as single event
                args[0] = events[i];
                //refire event
                this[action].apply( this, args );
            }

        },

        on : function( events ) {
            var args = arguments;

            //ensure that there are enough methods to continue
            //should have at least event and function
            if(args.length < 2){
                return;
            }

            //check if array of event slugs, and parse accordingly
            if(events.constructor === Array){
                this.parse('on', args );
                return;
            }

            //slug needs to be a string after it's passed parsing
            if(events.constructor != String){
                return;
            }

            //create event array if doesn't exist
            if(!this.events[events]){
                this.events[events] = [];
            }

            //get our args
            args = Array.prototype.slice.call(args);

            //loop through and add all functions to event array
            for(var i = 0, l = args.length; i < l; i++){
                //ensure our argument is a function before pushing
                //auto weeds our our first item ( event slug )
                if(args[i].constructor === Function){
                    this.events[events].push( args[i] );
                }
            }
        },

        off : function( events ){
            var args = arguments;
            var evnts;
            var pos;
            //ensure that there are enough methods to continue
            //should have at least event and function
            if(args.length < 2){
                return;
            }

            //check if array of event slugs, and parse accordingly
            if(events.constructor === Array){
                this.parse('on', args );
                return;
            }

            //slug needs to be a string after it's passed parsing
            //check if it exists as an event at the same time
            if(events.constructor != String || !this.events[events]){
                return;
            }

            evnts = this.events[events];

            //get our args
            args = Array.prototype.slice.call(args);
            //remove event
            args.shift();
            //loop through and add all functions to event array
            for(var i = 0, l = args.length; i < l; i++){
                //get index if exists
                pos = evnts.indexOf(args[i]);

                if(pos > -1){
                    this.events[events].splice( pos, 1 );
                }
            }


        },

        emit : function( events ){
            var args = arguments;
            var evts;

            if(!events){
                return;
            }

            //check if array of event slugs, and parse accordingly
            if(events.constructor === Array){
                this.parse('on', args );
                return;
            }

            //slug needs to be a string after it's passed parsing
            if(events.constructor != String){
                return;
            }

            //turn argument list into array
            args = Array.prototype.slice.call(args);

            if(this.events[events]){
                evts = this.events[events];
                args.shift();
                for(var i = 0, l = evts.length; i < l; i++){
                    evts[i].apply(this, args);
                }
            }

        },

        //remove all attached function from event
        clear : function( events ){

            if(!events){
                return;
            }

            //check if array of event slugs, and parse accordingly
            if(events.constructor === Array){
                this.parse('clear', arguments);
                return;
            }

            //slug needs to be a string after it's passed parsing
            if(events.constructor != String){
                return;
            }

            if(this.events[events]){
                this.events[events] = [];
            }
        }

    };

})(window);