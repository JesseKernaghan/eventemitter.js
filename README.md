EventEmitter.js Module
----

A simple event emitter module that is the basis for many of my other modules. While it doesn't have the broader range of functionality that other libraries have, it does the basics.

##Methods



###.on( [events], function, function.. )

Used to attach an unlimited amount of functions to an event or an array of events. 


#####Examples


Attach an anonymous function to 'foo' event:

     EventEmitter.on('foo', function(){
       console.log('bar');
     });
     

Attach function to multiple events at once:

     EventEmitter.on(['foo','bar'], function(){
     	console.log('baz');
     });
     
    
Attach multiple function references to an event:

     var foo = function(){
     	console.log('doing foo');
     };
     
     var bar = function(){
     	console.log('doing bar');
     };
     
     EventEmitter.on('baz', foo, bar);
     
   
>Note that you can use function references, anonymous functions, anything that ultimately leads to a function. However, in order to be able to detach the event later it is recommended to use a reference.


----

###.off( [events], function, function.. )

Used to detach an unlimited amount of functions to an event or an array of events. 


#####Examples


Detach a function from 'foo' event:

     var bar = function(){
     	console.log('doing bar');
     };
     //attach
     EventEmitter.on('foo', bar);
     
     //detach
     EventEmitter.off('foo', bar);
     
     

Detach function from multiple events at once:

     var baz = function(){
       console.log('doing baz');
     };
	  //attach
     EventEmitter.on(['foo','bar'], baz);
     
     //detach
     EventEmitter.off(['foo','bar'], baz);
     
    
Detach multiple function references from an event:

     var foo = function(){
     	console.log('doing foo');
     };
     
     var bar = function(){
     	console.log('doing bar');
     };
     
     //attach
     EventEmitter.on('baz', foo, bar);
     
     //detach
     EventEmitter.off('baz', foo, bar);
  

----

###.emit( [events], param, param... )

Fire the attached functions with provided parameters. An unlimited number of parameters are accepted, and multiple events can be fired at once.

#####Examples


Emit a single event without parameters:

     var bar = function(){
     	console.log('doing bar');
     };
     //attach
     EventEmitter.on('foo', bar);
     
     //fire
     EventEmitter.emit('foo'); //fires bar();
     
     

Emit a single event with multiple parameters

     var notify = function(name, email){
       console.log('User ' + name + ' updated successfully with email: ' + email);
     };
     
	 //attach
     EventEmitter.on(['new-user', 'update-user'], notify);
     
     //emit
     EventEmitter.emit('new-user', 'John', 'john@doe.com');
     //logs "User John updated successfully with email: john@doe.com"
     

Emit multiple events at once:

     var updateCart = function(item){
	     //...
     };
     
     var highlightItem = function(item){
    	 //...
     };
     
     //attach
     EventEmitter.on('add-item', updateCart);
     EventEmitter.on('update-view', highlightItem);
     
     //emit both events with same parameter
     EventEmitter.off(['add-item', 'update-view'], {[itemdata]} );
 

>Emitting multiple events at once is a rare case, since you probably want to pass unique parameters for each event.


----

###.clear( [events] )

Remove all functions from event or events.

#####Examples


Remove all functions from single event:

     EventEmitter.clear('foo'); 
     //removes all functions attached to foo event
     
     

Remove all functions from multiple events:

     EventEmitter.clear(['foo','bar']);
     //removes all functions attached to foo and all attached to bar
     




