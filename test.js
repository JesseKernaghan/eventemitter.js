(function(w){

    var foo = function(){
        console.log('foo fired:');
        console.log(arguments);
    };

    var bar = function(){
        console.log('bar fired:');
        console.log(arguments);
    };


    EventEmitter.on(["event", "foo"], foo, bar);

    console.log('functions attached to "event"');
    console.log(EventEmitter.events["event"]);
    console.log('functions attached to "foo"');
    console.log(EventEmitter.events["foo"]);

    EventEmitter.clear(["event", "foo"]);

    console.log('functions attached to "event"');
    console.log(EventEmitter.events["event"]);
    console.log('functions attached to "foo"');
    console.log(EventEmitter.events["foo"]);

})(window);